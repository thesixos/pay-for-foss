% HOWTO Pay for Free Software
% Mary Gardiner
% 4 March 2003


Introduction
============

This document is a guide to how to pay for your Free Software. A lot of people have put in a lot of time to make your high quality Free Software. They chose to licence it so that you can modify and redistribute it. They often, although not always, gave you the program without requiring that you pay any money for it.

Despite being cheap to obtain the development, testing, and maintenance of the Free Software you use is not negligible in cost, far from it. But unlike proprietary, commercial software, there are ways you can pay for Free Software that aren't money.

You can pay for Free Software by contributing time, money or resources to Free Software projects.

Many of the developers of existing Free Software have sufficient incentive to develop Free Software. Their reasons are various. Some developers believe strongly in freely shared intellectual property. Others simply enjoy the process of creating software.

This document, however, is aimed at users of Free Software who do not find either shared intellectual property or the creation of software itself a compelling reason to participate in Free Software development. These users appreciate existing Free Software for its quality, the ability to modify it, its support communities, or its availability compared to proprietary alternatives.

Some of these users are not confident in their ability to contribute. The most common reason these users give is that they can't program, or can't program well, but others include not knowing how to become involved in the community, or not knowing how to contribute to a well established project. I hope that they will find some alternative ways to contribute in this HOWTO.

Some of these users do not feel their contribution is necessary. They believe that the Free Software community is mature enough that it can meet their demands for quality Free Software without their involvement. I hope that this HOWTO will convince them both that their involvement will make Free Software better, and that their involvement is likely to give others incentive to help them with their particular needs.

Free software is Free, right?
-----------------------------

Here's what's free about it: redistribution and modification. You are free to redistribute the program. You are free to modify the source code and distribute your modifications, at least under the original licence, and sometimes under other licences too.

Here's what's not free about it: development, testing, maintenance and distribution. Someone's time, or money, or bandwidth, or hard-drive space was consumed.

But, it's effectively Free, right?
----------------------------------

Free software is often, but not always, distributed or available free of a purchase price. This unfortunately leads some users to believe that it was developed at no cost to the developers, and that modifications come at no cost to the developers.

There are several incentives to contribute to Free Software. For people who don't find the community or the work sufficient incentive, a considerable incentive is producing better software. Many users of Free Software, whether individuals or corporate users, have needs that are not met by existing Free Software. Often, these needs are not met by any available software at all.

Inspiring proprietary software developers to develop a product or add a feature is fairly straightforward. The incentive for proprietary software developers is money, and if they are persuaded that the prospect of making money outweighs development time and various risks, someone may well develop your software.

Inspiring Free Software developers is not so straightforward. Increased sales, or other monetary incentives may be possible. Alternatively they might need to be convinced that your feature or program sounds fun, that it is an intelligent way to do things, that it won't take much time or that you'll help.

Some Free Software users do not seem to realise that Free Software developers have no more obligation to their users to develop or support software than proprietary software developers have obligation for unpaid support or development. Demanding quality Free Software without helping build it is at best useless, at worst counterproductive.

I hope this HOWTO will illustrate that contributing to Free Software is the most likely way to get Free Software that meets your needs. 

HOWTO contribute money
======================

Not every Free Software project needs money. Some Free Software projects are developed and maintained after hours by a single person or a small team who have paying jobs, and they are happy to continue donating their time to the development. Others projects are already supported by individuals, organisations, or companies with the means to do so.

On the other hand, there are many projects who right at this moment are reaching the size where they are becoming a large job for someone, and that someone cannot yet give up their paying work to develop Free Software full-time. There are other projects maintained by people are are fairly poor, and who would appreciate donations because they do not have paid work. There are developers who would simply appreciate money.

There are also companies trying to make a profit supporting Free Software and either selling the software or associated support or products. These companies also employ Free Software developers and ensure the continuance of Free Software projects crucial to their business.

Perhaps you personally cannot afford to buy much Free Software, or contribute any money to Free Software. Nevertheless, you may have the opportunity to convince others to buy Free Software. Perhaps you consult for a company who would benefit from a support contract for Free Software. It might be that you work for a company that makes extensive use of a Free Software project, and would benefit from financially supporting that project's continued development.

Remember, you do get something in return for your money when you contribute to a Free Software project. It may be the continuance of the project, or it may be something else - support, maintenance, customisations and so on.

Give a developer, project or company some money
-----------------------------------------------

Some developers and projects solicit donations, simply follow their instructions and donate a sum of money.

Others may not solicit donations, but might accept them if offered. Contact them and offer a donation. For some developers, accepting donations legally might be more trouble than it is worth, but assume that they will tell you this if it is the case.

Donating money will directly benefit whichever project, large or small, that you donate money to.

Buy a Free Software product, or associated products
---------------------------------------------------

Buying a Free Software product might mean buying your product on CD rather than downloading or copying it, or paying to download the product. This money will help the seller maintain a Free Software business, and, particularly in the case of Free Software companies, will pay the salaries of their in-house developers.

Buying an associated product means buying an additional service provided by the developers or maintainers: for example, support or documentation. Many Free Software companies have business models that primarily depend on selling associated products, particularly support.

Buying a Free Software product or associated products en masse (e.g., a distribution) will help support companies producing or supporting Free Software, and developers of large projects (kernels, desktop environments and the like). Buying a product from an individual project supports that project directly, but probably not others.

Hire Free Software developers
-----------------------------

If you rely extensively on a particular piece of Free Software, you could hire developers of that piece of software to continue work on it. These developers will also be ideally placed to customise and develop extensions to the project to suit your needs. You may also choose, or the licence may require, that these additions become Free Software, in which case you are also contributing skills to Free Software.

Even if you do not need a developer permanently you might consider occasionally hiring one. If you need a particular feature added to a project you use, you could hire one of the developers to work on that feature.

Free Software is developed for all sorts of reasons, which include enjoying software development, and monetary incentive, but probably don't align perfectly with your particular needs. Paying for the development of Free Software will mean that the software you are using better suits your needs than the project otherwise might.

Support Free Software conferences
---------------------------------

Conferences give developers time together. Many developers, working on a single project but scattered all over the globe, are seldom able to meet with their peers. Thus, conferences are invaluable to many projects.

Free Software conferences often pay the travel expenses and accommodation costs for speakers and workshop participants. They often also subsidise students and unemployed attendees. They cover the costs by charging attendees, and by accepting donations. A Free Software conference can be very expensive to run.

If some of the talks at a Free Software conference would be useful to you, consider paying to attend the conference. If you do not wish to attend, consider a donation. If the conference relates to a particular project you use, consider sponsoring the conference, or a workshop on that project.

The talks at a Free Software conference are only one of the reasons for attending. You will meet other users and developers in person. If you want to be part of a Free Software community, one of the best ways to meet the community face-to-face is at a conference.

Sponsoring a Free Software conference might earn you considerable community goodwill, as well as contacts in the community.

Commission Free Software
------------------------

If you need to commission a particular piece of software or produce software in-house, consider releasing it as Free Software, or specifying in the contract that it must be released as Free Software.

Not only will this mean that you have contributed Free Software to the world, you may also receive contributions to your software from external developers.

Buy hardware from companies that support Free Software
------------------------------------------------------

Some companies sell hardware that runs, or is designed to run, Free Software. Consider supporting them when buying hardware. Be sure to do research first - many companies are only beginning to support Free Software and need your support. 

HOWTO contribute resources
==========================

Contribute hardware
-------------------

Many Free Software projects are designed to run on many different types of hardware, at many different scales.

It is rare, however, that the developers of these projects have access to all the different varieties of hardware that their project should run on. If you have unused hardware with unusual components, or an unusual architecture, that a Free Software project would like to support, donate the hardware. If you need a Free Software project to run on a particular configuration you have, you might like to donate an identical system for development and testing.

Contribute bandwidth
--------------------

Developers and users of Free Software require a large amount of bandwidth. Projects are generally coordinated through web pages, source repositories, mailing lists, news groups and IRC channels. Free Software is often distributed by large mirror sites that require a lot of bandwidth. Mirror sites are particularly useful for developers and users in countries outside the USA, for whom international traffic is slow or expensive. They also significantly reduce the bandwidth use at the primary site.

If you have access to a reliable high bandwidth connection, and can afford the traffic fees, you might consider hosting mirror sites. Most existing sites have directions about how to become a mirror.

If you have access to a significant amount of bandwidth, but perhaps not the amount required to host a mirror, you might still be able to provide bandwidth to projects or user groups by hosting their websites and mailing lists, or by becoming a node in a Free Software IRC network, or similar. Note that in this case most groups will want reliable access to server administration. Many may prefer to handle server administration tasks themselves, as this will allow them to fix problems fairly quickly.

HOWTO contribute time and skills
================================

Write some Free Software
------------------------

Most Free Software products do not exist until someone, somewhere, writes some code. If you can program you can contribute code to an existing product or you can write your own Free Software product.

It can be difficult to decide where and how to contribute code. As a guide, it takes a while know a large product well enough to be able to significantly contribute. Large products also have many developers, and plans for development which they are unlikely to change for your benefit. On the other hand, the product's community will also be larger, and there may be formal or informal mentoring for new developers. It may also be divided into smaller projects which you can hack on without knowing the entire product's codebase.

A smaller product is more likely to desperately need new developers. You are more likely to be able to develop crucial parts of the product, and have some say in the direction of development. On the other hand, a smaller product's community is also smaller, and may not have the same momentum as a larger community.

Starting your own product is risky. You will need to build a developer community and a user community, as well as develop a product. You will also need to personally maintain the community's momentum for some time, possibly the lifetime of the product. However, if there are no existing Free Software products that have or could have the features you want, you're probably not the only person who would contribute to your new product.

Write some documentation for Free Software
------------------------------------------

Contribute some artwork to Free Software
----------------------------------------

Many Free Software projects need polished artwork, including icons, background images, colour schemes and logos. At present there is comparatively little freely licensed artwork. If you are an artist, your skills would be appreciated by many Free Software projects. You could also freely licence your artwork to be used in other projects.

Many projects also need the skills of composers, whether composing short pieces of sound or extensive soundtracks for their program. Again, you could freely license your music for distribution and modification. projects.

Do some translation or internationalisation for Free Software
-------------------------------------------------------------

Many large Free Software projects are sorely in need of competent translations into other languages. If you are a clear writer and can read and write more than one language, you could offer your services translating documentation or text inside the program into another language.

There are several other problems in internationalisation that you might help a Free Software project solve. They include the direction of text, the cultural meaning of and appropriateness of any signs or symbols used, and local standards of modesty or decency that might require modification of language or images used in projects. If you live in a culture other than the culture of the developers of the project, your feedback about cultural appropriateness would be particularly valuable.

Report bugs in Free Software
----------------------------

All software has faults, errors, flaws, and missing features. Most Free Software projects solicit bug reports, and sometimes feature requests, from the public.

When reporting a bug in software, you need to provide a clear description of the problem, including the exact wording of any error messages the program gives, and the exact steps needed to cause the error to happen.

Be careful to report the bug to the right place, in order to make sure that it can be tracked by the appropriate developers. You should also check if your bug has been reported by someone else. If it has, you only need to contribute your own report if you think you have more information, or a fix for the bug. Make sure you reference the original bug report somehow.

When reporting a bug, first check if the project has an official bug reporting website or email address. Normally these give very clear and precise instructions on reporting bugs.

If there aren't specific instructions for reporting bugs, you should attempt to report them to the developer's group, forum or list if there is one, and to a particular developer if there is not. Don't send bugs to a particular developers unless you can't find another way to report them as they might be too busy to make sure your bug receives attention.

A clear bug report is the best way to ensure that a bug gets investigated and fixed. The more clear and precise your bug report is, the easier it is to fix. Bug reports that include a suggested fix are particularly welcome.

Test some Free Software
-----------------------

Every software product is improved by extensive testing. Free Software is no exception. Download testing versions of a project. Try and cause them to break. Report how they broke (see bug reporting above).

Larger projects will have coordinated testing teams. Join these teams and ask to be assigned some testing.

Contribute specialised skills or knowledge to Free Software
-----------------------------------------------------------

Even if you don't have skills that are directly useful in getting a polished, quality Free Software project out the door, perhaps you have skills that a Free Software project could use in less direct ways.

Examples include professional skills such as accountancy or law. Large Free Software projects often need advice about legal issues or financial issues including intellectual property concerns, accepting donations or taxation issues. Since this advice is normally comparatively expensive, providing your services for free or at a reduced rate could be valuable to a project.

Some projects, conferences and groups market themselves to the press and public, if you have skills in marketing, advertising or public relations these would be invaluable to many projects. Many projects have teams in place to take care of marketing, so even if you cannot assume responsibility for marketing a project, you could play an equally valuable role sharing your advice and experience with members of the team.

Many Free Software projects don't have members experienced in project management. As a project grows larger, it can benefit from competent project management. As with marketing skills, teaching these skills to other members of the project is as useful a contribution as becoming a project manager.

Many Free Software projects are working on software designed for professionals in a particular field - if you work in that field your advice as a user, designer and tester would be particularly useful. 

HOWTO contribute support
========================

One of the biggest selling points of Free Software is the large amount of community support provided for it. This support takes the support load off developers and project members allowing them to devote more time to development. If you cannot contribute to the project itself, contributing support to that project helps both its developers and its users.

Many support groups run on the same principles as Free Software projects. The members are often unpaid and undervalued volunteers. They owe you nothing. The best way to get help from a support group is giving help to others when they need it, and doing your own research as much as possible rather than relying on the support group unnecessarily. Eric S. Raymond's and Rick Moen's tips on [How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html) might help you frame your questions better.

Join (or start) a user group for Free Software
----------------------------------------------

You can find user groups in many cities and towns, particularly for large projects, or for Free Software in general. These user groups often hold regular meetings, and hold talks or demonstrations related to the project. They often help publicise the project locally, encourage its use in business, and may provide support forums.

Many projects have online user groups, often in the form of a mailing list. These user groups generally provide support, but occasionally also

If there is an existing user group, you can join it. At the least, you can contribute to support forums, or participate in meetings or demonstrations. Many users groups are run by individuals or small groups who would appreciate your help. You could also volunteer as a speaker or demonstrator, help with administrative tasks such as mailing list management or meeting organisation or help publicise the group and its events.

If there is no existing user group, consider starting one. Starting a user group requires an investment of time and resources. Advertise first and make sure there is sufficient interest to sustain a group. You might want to contact the developers of the project, who might know of other groups or possible members.

Contribute to (or start) an online support group
------------------------------------------------

Online support forums are quite similar to online user groups, and the two often coincide. Online support forums exist specifically to answer questions about installing and using a product, or a group of products. Many of the questions and answers are archive for future reference. Every question you answer on an online forum is a question a busy developer doesn't need to answer. Every question you ask on an online forum is a question whose answer will be archived for future use. 

HOWTO contribute appreciation
=============================

Express your appreciation to a developer.
-----------------------------------------

Much of the feedback received by many Free Software projects is negative. Many developers feel discouraged and undervalued. Positive feedback and appreciation for their hard work is rare, and highly prized.

Emailing Free Software developers is normally easy. Their email address is often advertised in documentation, source code and mailing lists. On rare occasions they might advertise a phone number or postal address instead. Use whichever contact details are easiest to find.

Give a project or developer a gift
----------------------------------

Some projects will mention in the documentation that the developer or project would appreciate a particular gift. Often this gift will be cheap, for example, a postcard or CD. Consider buying it for them. If they do not name gifts, ask them what they would like.

Do your research when using Free Software
-----------------------------------------

Some developers of Free Software, particularly popular projects, receive an immense amount of mail requesting help. Many user groups see the same questions over and over again. In order to save them time and frustration, and in order to give them more time to work on their project or to support other users, consult relevant HOWTOs, Frequently Asked Questions, mailing list archives and manuals as appropriate before asking for help.

Make your favourite project look good.
--------------------------------------

Take an active role in the user groups of your favourite project and actively make sure you leave a good impression on potential users and developers, both of your project and of other Free Software projects. Be as helpful as you can when they have difficulties. Be honest about the strengths and weaknesses of your project. Constructively criticise your project, and other projects.

Use word of mouth to publicise your project. Research other competing projects and draw sensible, fair comparisons. Highlight any advantages you have. Make one of those advantages that you have a wonderful community of developers and users around your project. 

Acknowledgements
================

This HOWTO was inspired by an extensive discussion on the
[LinuxChix](http://www.linuxchix.org/) mailing lists. Thanks to LinuxChix and
also to the [Sydney Linux Users Group](http://www.slug.org.au/) and members of
[Advogato](http://www.advogato.org/) for their feedback.

Thanks to Rebecca Walter, Jenn Vesperman, Dancer Vesperman, Telsa Gwynne and Alan Cox for providing some specific ideas, and Akkana Peck for suggesting that users who demand features without offering contributions need to be specifically addressed. Thanks to David Merrill for his critical eye and Jeff Waugh for his enthusiasm. 

The meaning of Free.
====================

The precise meaning of Free Software is difficult to encapsulate. Free Software is often distributed for free, but the term "Free" refers to freedom of use, freedom to redistribute and freedom to modify.

You can find out more about the definitions of Free Software from the Free Software Foundation's document [The Free Software Definition](https://www.gnu.org/philosophy/free-sw.html).

[The Open Source Definition](https://opensource.org/osd) is based on similar principles. 

About this document
===================

This article is by [Mary Gardiner](https://mary.gardiner.id.au/).

It was originally authored in 2002–2003, and intended for [The Linux
Documentation Project](http://www.tldp.org/) but that goal was abandoned. No
further revisions are planned, other than typographical fixes or fixes to link
targets. [The article's Markdown source is available on
Gitlab](https://gitlab.com/puzzlement/pay-for-foss) for copying, forking and
releasing derivative works under Creative Commons Attribution-Sharealike.

The preferred link for this document is
[https://files.puzzling.org/wayback/pay-for-foss/](https://files.puzzling.org/wayback/pay-for-foss/)

<section id="licence"><a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="https://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">HOWTO Pay for Free Software</span> by <a xmlns:cc="https://creativecommons.org/ns#" href="https://mary.gardiner.id.au/" property="cc:attributionName" rel="cc:attributionURL">Mary Gardiner</a> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</section>
